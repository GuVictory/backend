# CodesFord VkApp Backend

-   Серверная часть для приложения VkApp
-   Version 1.0

## Инструкция по запуску

1. Клонировать проект с GitHub и перейти в дерикторию проекта

    ```bash
    git clone https://GuVictory@bitbucket.org/GuVictory/backend.git
    ```

    ```bash
    cd backend
    ```

2. Выдать права .sh скрипту из дериктории проекта

    ```bash
    chmod +x ./entrypoint.sh
    ```

3. Перед сборкой контейнера необходимо создать файл .env в корне проекта со структурой:

    ```
    DEBUG=1
    ```

    ```
    SECRET_KEY=super_secret_key
    ```

    ```
    DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1]
    ```

    ```
    SQL_ENGINE=django.db.backends.postgresql_psycopg2
    ```

    ```
    SQL_DATABASE=vk_app_codefords
    ```

    ```
    SQL_USER=vk_app_codefords_user
    ```

    ```
    SQL_PASSWORD=vk_app_codefords_pass
    ```

    ```
    SQL_HOST=db
    ```

    ```
    SQL_PORT=5432
    ```

    ```
    DATABASE=postgres
    ```

4. Сборка и запуск Docker контейнера

    ```bash
    docker-compose -f docker-compose.prod.yml up -d --build
    ```

    Далее сервис будет доступен по http://localhost:8000/api/

5. Для остановки контейнера в текущей дериктории выполнить
    ```bash
    docker-compose down
    ```

---

## При необходимости создания superuser

1. Узнать id запущенного Docker контейнера
    ```bash
    docker ps -aqf "name=backend_web_1"
    ```
2. Запустить создание суперпользователя c полученный container_id
    ```bash
    docker exec -it container_id  python manage.py createsuperuser
    ```

---

## Документация к API

### Пользователи

#### Регистрация:

-   Url: /users/register
-   Method: POST
-   Request body:

```json
{
    "email": "hello@example.com",
    "password": "VerySafePassword0909",
    "first_name": "John",
    "last_name": "Howley"
}
```

-   Response status 201 and body:

```json
{
    "id": 1,
    "first_name": "John",
    "last_name": "Howley",
    "email": "hello@example.com",
    "is_active": true,
    "is_staff": false,
    "is_superuser": false,
    "auth_token": "34303fc8c5a686f2e21b89a3feff4763abab5f7e"
}
```

-   Response status 400 and body:

```json
{
    "email": ["user with this email already exists."]
}
```

---

#### Авторизация

-   Url: /users/login
-   Method: POST
-   Request body:

```json
{
    "email": "hello@example.com",
    "password": "VerySafePassword0909"
}
```

-   Response status 200 and body:

```json
{
    "id": 1,
    "first_name": "John",
    "last_name": "Howley",
    "email": "hello@example.com",
    "is_active": true,
    "is_staff": false,
    "is_superuser": false,
    "auth_token": "34303fc8c5a686f2e21b89a3feff4763abab5f7e"
}
```

-   Response status 400 and body:

```json
["Invalid username/password. Please try again!"]
```

---

#### Выход пользователя

-   Url: /users/logout
-   Method: POST

-   Response status 200 and body:

```json
{
    "success": "Sucessfully logged out"
}
```

---

#### Смена пароля:

-   Url: /users/password_change
-   Method: POST
-   Request body:

```json
{
    "current_password": "NotSoSafePassword",
    "new_password": "VerySafePassword0909"
}
```

-   Response status 204 No-Content:

-   Response status 400 and body:

```json
{
    "current_password": ["current_password not correct"]
}
```


---

### Карточки для английского

#### Получение всех карт:

-   Url: /eng/
-   Method: GET

-   Response status 200 and body:

```json
[
  {
    "id": 2,
    "in_english": "clon",
    "in_russian": "клонировать"
  },
  {
    "id": 3,
    "in_english": "bye",
    "in_russian": "пока"
  }
]
```

---

#### Получение карточки по id

-   Url: /eng/<int:id>
-   Method: GET

-   Response status 200 and body:

```json
{
  "id": "id",
  "in_english": "clon",
  "in_russian": "клонировать"
}
```

-   Response status 404 and body:

```json
{
  "detail": "Not found."
}
```

---

#### Создание карточки

-   Url: /eng
-   Method: POST
-   Для отправки необходимо авторизоваться пользователю со статустов is_eng_tutor = True
-   Request body:

```json
{
  "in_english": "dog",
  "in_russian": "собака"
}
```

-   Response status 201 and body:

```json
{
  "in_english": "dog",
  "in_russian": "собака",
  "id": 4
}
```

-   Response status 403 and body:

```json
{
  "message": "You have no permissions!"
}
```

---

#### Обновление данных карточки

-   Url: /eng
-   Method: PUT
-   Для отправки необходимо авторизоваться пользователю со статустов is_eng_tutor = True
-   Request body:

```json
{
  "in_english": "dog",
  "in_russian": "собака"
}
```

-   Response status 201 and body:

```json
{
  "in_english": "dog",
  "in_russian": "собака",
  "id": 4
}
```

-   Response status 403 and body:

```json
{
  "message": "You have no permissions!"
}
```

---

#### Удаление карточки

-   Url: /eng/<int:id>
-   Method: DELETE

-   Response status 204 - успешно удален

-   Response status 404 - не найден

-   Response status 403 and body:

```json
{
  "message": "You have no permissions!"
}
```

---