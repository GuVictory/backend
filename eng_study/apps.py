from django.apps import AppConfig


class EngStudyConfig(AppConfig):
    name = 'eng_study'
