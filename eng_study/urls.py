from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from .views import EngRusCardViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('', EngRusCardViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
