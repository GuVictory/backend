from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework import viewsets, status

from .serializers import EngRusCardSerializer
from .models import EngRusCard


class EngRusCardViewSet(viewsets.ModelViewSet):
    queryset = EngRusCard.objects.all()
    serializer_class = EngRusCardSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        cards = EngRusCard.objects.all()
        serializer = EngRusCardSerializer(cards, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        user = request.user

        if not user.is_eng_tutor:
            err_msg = {'message': 'You have no permissions!'}
            return Response(err_msg, status=status.HTTP_403_FORBIDDEN)

        serializer = EngRusCardSerializer(request.data, many=False)
        serializer = serializer.data
        card = EngRusCard(
                in_english=serializer['in_english'],
                in_russian=serializer['in_russian'],
        )

        card.save()
        serializer['id'] = card.id
        return Response(serializer, status=status.HTTP_201_CREATED)


    def update(self, request, pk=None):
        if not request.user.is_eng_tutor:
            err_msg = {'message': 'You have no permissions!'}
            return Response(err_msg, status=status.HTTP_403_FORBIDDEN)

        try:
            card = EngRusCard.objects.get(id=pk)
            serializer = EngRusCardSerializer(request.data, many=False)
            serializer = serializer.data

            if 'in_english' in serializer:
                card.in_english = serializer['in_english']

            if 'in_russian' in serializer:
                card.in_russian = serializer['in_russian']

            card.save()

            serializer = EngRusCardSerializer(card, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except:
            err_msg = {'message': f'No card with id {pk}!'}
            return Response(err_msg, status=status.HTTP_404_NOT_FOUND)


    def destroy(self, request, pk=None):
        if not request.user.is_eng_tutor:
            err_msg = {'message': 'You have no permissions!'}
            return Response(err_msg, status=status.HTTP_403_FORBIDDEN)

        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)