from rest_framework import serializers
from .models import EngRusCard


class EngRusCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = EngRusCard
        fields = ['id', 'in_english', 'in_russian']
