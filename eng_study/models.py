from django.db import models


# Create your models here.
class EngRusCard(models.Model):
    in_english = models.CharField(max_length=256, default='', blank=True)
    in_russian = models.CharField(max_length=256, default='', blank=True)

    def __str__(self):
        return self.in_english
