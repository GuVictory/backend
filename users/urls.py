from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from .views import AuthViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('', AuthViewSet,  basename='auth')

urlpatterns = [
    path('', include(router.urls)),
]
