from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from rest_framework import serializers


def get_and_authenticate_user(email, password):
    user = authenticate(username=email, password=password)
    if user is None:
        raise serializers.ValidationError("Invalid username/password. Please try again!")
    return user

def create_user_account(email, password, first_name="",
                        last_name="", is_tutor=False, is_eng_tutor=False, **extra_fields):
    user = get_user_model().objects.create_user(
        email=email, password=password, first_name=first_name,
        last_name=last_name, is_tutor=is_tutor, is_eng_tutor=is_eng_tutor, **extra_fields)
    return user